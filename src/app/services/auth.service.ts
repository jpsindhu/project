import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders  } from '@angular/common/http';
import { Observable } from 'rxjs';
const API_URL: string = "https://devgroceryapi.spericorn.com/api/";
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  constructor(public http: HttpClient) { }
  httpHeader = {
    headers: new HttpHeaders(
      { 
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6OSwidXNlcm5hbWUiOiJBc2hvayIsImVtYWlsIjoiYXNob2tAc3Blcmljb3JuLmNvbSIsInJvbGUiOjQsImlhdCI6MTU5MTYwMDkxNywiZXhwIjoxNTkzMjc4OTE4NjA5fQ.O38c12nGe9MHTR6i9KPy8SzQYBbrJ10FxEwTcO25JQ0',
      }
      )
  };
  register(data) {
    return this.http.post(API_URL + "auth/register", data,this.httpHeader);
  }

  login(data) {
    return this.http.post(API_URL + "auth/login", data,this.httpHeader);
  }
  profile(){
    return this.http.get(API_URL + 'user',this.httpHeader);
  }
  emailCheck(email){
    return this.http.post(API_URL + "auth/checkMail", email,this.httpHeader);
  }
}
