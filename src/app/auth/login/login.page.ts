import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";
import { AuthService } from './../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
 public isLogged: Boolean = false;
 public userData;
  constructor(private  authService:  AuthService, private  router:  Router) { }

  ngOnInit() {
  }
  login(form){
    this.authService.login(form.value).subscribe((res)=>{
      this.userData= res;
      if(this.userData.success){
        this.isLogged = true;
        this.router.navigateByUrl('profile');
      }else{
        this.router.navigateByUrl('login');
      }
    }, (error) => {

      this.router.navigateByUrl('home');
    });
  }
}
