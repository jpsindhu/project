import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";
import { AuthService } from './../../services/auth.service';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  constructor(private  authService:  AuthService, private  router:  Router) { }
  public profileData: any;
  public user: any;
  ngOnInit() {
    this.getProfile();
  }
  getProfile() {
    this.authService.profile().subscribe((res) => {
       this.profileData = res;
       this.user =this.profileData .data.userData;
    });
  }
}
